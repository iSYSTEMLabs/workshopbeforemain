# WorkshopBeforeMain

Before `main()` workshop material

...for workshop based on [Before main() presentation](Before%20main()%20presentation.pdf)

STM32F303 Nucleo board used for hands on work sponsored by STMicroelectronics
![alt text](stlogo.png)

Get [ARM GCC compiler](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads) to use in this demo and point to it in winIDEA project settings.

![alt text](iSYSTEM_Logo.jpg)