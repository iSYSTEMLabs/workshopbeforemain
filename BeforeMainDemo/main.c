//////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 iSYSTEM Labs                                                  //
//                                                                                  //
// Permission is hereby granted, free of charge, to any person obtaining a copy     //
// of this software and associated documentation files (the "Software"), to deal    //
// in the Software without restriction, including without limitation the rights     //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell        //
// copies of the Software, and to permit persons to whom the Software is            //
// furnished to do so, subject to the following conditions:                         //
//                                                                                  //
// The above copyright notice and this permission notice shall be included in all   //
// copies or substantial portions of the Software.                                  //
//                                                                                  //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR       //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,         //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE      //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER           //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,    //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE    //
// SOFTWARE.                                                                        //
//////////////////////////////////////////////////////////////////////////////////////
extern unsigned long _estack, _etext, _sdata, _edata, _sbss, _ebss;

void SomeCall();

void delay();

int g_iInitialized = 7;
int g_iNotInitilized;

#define RCC_AHBENR                              (*(unsigned long *)(0x40021000 + 0x14))
#define GPIOB_MODER                             (*(unsigned long *)(0x48000400 + 0x00))
#define GPIOB_OTYPER                            (*(unsigned long *)(0x48000400 + 0x04))
#define GPIOB_ODR                               (*(unsigned long *)(0x48000400 + 0x14))

#define DELAY_LOOPS                             (unsigned long )             (0x100000)

void main()
{
  SomeCall();
  
  RCC_AHBENR = (RCC_AHBENR & 0xFFFBFFFF) | 0x00040000;
  GPIOB_MODER = (GPIOB_MODER & 0xFFFFFF0F) | 0x00000040;
  GPIOB_OTYPER = (GPIOB_OTYPER & 0xFFFFFFF7);
  GPIOB_ODR = (GPIOB_ODR | 0x8);

	while(1)
  {
    GPIOB_ODR ^= 0x00000008;    
    delay();
  }
}

void delay()
{
  unsigned long i = DELAY_LOOPS;
  while(i--);
}

void reset()
{
  unsigned long *pdwSource = &_etext;
  unsigned long *pdwDestination = &_sdata;
  unsigned long *pdwEnd = &_edata;
  
  while ( pdwDestination < pdwEnd )
  {
    *pdwDestination = *pdwSource;
    pdwDestination++;
    pdwSource++;
  }
  
  pdwDestination = &_sbss;
  pdwEnd = &_ebss;
  while ( pdwDestination < pdwEnd )
  {
    *pdwDestination = 0;
    pdwDestination++;
    pdwSource++;
  }
  
  main();
  while(1);
}

__attribute__((section(".vectors")))
const unsigned long g_adwVectors[] =
{
  (unsigned long)&_estack,
  (unsigned long)reset
};
